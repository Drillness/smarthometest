package com.test.smarthome.presentation.main.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import com.test.smarthome.R
import com.test.smarthome.databinding.FragmentProfileBinding
import com.test.smarthome.databinding.FragmentRollerControlBinding
import com.test.smarthome.presentation.main.dev_details.roller.RollerViewModel
import com.test.smarthome.util.InputUtils
import com.test.smarthome.util.getViewModelFactory

class ProfileFragment : Fragment() {

    private lateinit var binding: FragmentProfileBinding
    private val viewModel by viewModels<ProfileViewModel> { getViewModelFactory() }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        binding = FragmentProfileBinding.inflate(layoutInflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.user.observe(viewLifecycleOwner, Observer {
            with(binding) {
                etUserName.setText(it.firstName)
                etSurname.setText(it.lastName)
                etCountry.setText(it.addressResponse.country)
                etCity.setText(it.addressResponse.city)
                etPostalCode.setText(it.addressResponse.postalCode.toString())
                etStreet.setText(it.addressResponse.street)
                etStreetCode.setText(it.addressResponse.streetCode)
                etBirthdate.setText(it.birthDate.toString())
            }
        })

        binding.btnSave.setOnClickListener {
            save()
        }
    }

    fun save() {
        viewModel.saveProfile()
    }

    override fun onStart() {
        super.onStart()
        viewModel.getProfile()
    }
}