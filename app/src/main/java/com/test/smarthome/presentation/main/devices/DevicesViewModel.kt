package com.test.smarthome.presentation.main.devices

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.test.smarthome.data.models.Result
import com.test.smarthome.domain.FetchDevicesUseCase
import com.test.smarthome.domain.RemoveDevicesUseCase
import kotlinx.coroutines.launch

class DevicesViewModel(
    private val fetchDevicesUseCase: FetchDevicesUseCase,
    private val deleteDevicesUseCase: RemoveDevicesUseCase
) : ViewModel(), DevicesAdapter.ItemDeleteListener {

    val devAdapter = DevicesAdapter(delListener = this)

    fun fetchDevices() {
        viewModelScope.launch {
            val taskResult = fetchDevicesUseCase()
            if (taskResult is Result.Success) {
                devAdapter.submitList(taskResult.data)
            } else {
                devAdapter.submitList(emptyList())
            }
        }
    }

    override fun deleteDevice(position: Int) {
        viewModelScope.launch {
            deleteDevicesUseCase(devAdapter.getItemAt(position))
            fetchDevices()
        }
    }
}