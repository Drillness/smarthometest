package com.test.smarthome.presentation.main.devices

import android.view.*
import androidx.appcompat.widget.PopupMenu
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.recyclerview.widget.RecyclerView
import com.test.smarthome.R
import com.test.smarthome.data.models.Device
import com.test.smarthome.data.models.Heater
import com.test.smarthome.data.models.Light
import com.test.smarthome.data.models.RollerShutter
import com.test.smarthome.databinding.ItemDeviceBinding

class DevicesAdapter(
    private var devices: List<Device> = emptyList(),
    private var delListener: ItemDeleteListener
) : RecyclerView.Adapter<DevicesAdapter.ViewHolder>() {

    fun submitList(items: List<Device>) {
        devices = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(devices[position], position, delListener)
    }

    override fun getItemCount(): Int = devices.size

    fun getItemAt(position: Int) = devices[position]

    class ViewHolder(private val viewBinding: ItemDeviceBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {

        fun bind(dev: Device, position: Int, delListener: ItemDeleteListener) {
            val resImage: Int
            val destination: NavDirections
            when (dev) {
                is Heater -> {
                    resImage = R.drawable.ic_smart_heating
                    destination = DevicesFragmentDirections.actionDevicesToHeat(dev.id.toString())
                }

                is Light -> {
                    resImage = R.drawable.ic_light_bulb
                    destination = DevicesFragmentDirections.actionDevicesToLight(dev.id.toString())
                }

                is RollerShutter -> {
                    resImage = R.drawable.ic_roller
                    destination =
                        DevicesFragmentDirections.actionDevicesToRoller(dev.id.toString())
                }

                else -> {
                    resImage = R.drawable.ic_unknown
                    destination =
                        DevicesFragmentDirections.actionDevicesToDevDetails(dev.id.toString())
                }
            }

            viewBinding.apply {
                ivDevIcon.setImageDrawable(
                    ContextCompat.getDrawable(viewBinding.root.context, resImage)
                )
                tvDevId.text = dev.id.toString()
                tvDevName.text = dev.deviceName

                cvRoot.setOnClickListener(
                    createOnClickListener(viewBinding, dev.id.toString(), destination)
                )
                ibDelete.setOnClickListener {
                    showPopupMenu(viewBinding.ibDelete, position, delListener)
                }
            }

            ViewCompat.setTransitionName(viewBinding.tvDevId, "id_${dev.id}")
            ViewCompat.setTransitionName(viewBinding.tvDevName, "name_${dev.id}")
            ViewCompat.setTransitionName(viewBinding.ivDevIcon, "icon_${dev.id}")
            ViewCompat.setTransitionName(viewBinding.cvRoot, "root_${dev.id}")

        }

        private fun createOnClickListener(
            binding: ItemDeviceBinding, devId: String,
            action: NavDirections
        ):
                View.OnClickListener {
            return View.OnClickListener {
                val extras = FragmentNavigatorExtras(
                    binding.tvDevId to "id_${devId}",
                    binding.tvDevName to "name_${devId}",
                    binding.ivDevIcon to "icon_${devId}",
                    binding.root to "root_${devId}"
                )
                it.findNavController().navigate(action, extras)
            }
        }

        private fun showPopupMenu(view: View, position: Int, delListener: ItemDeleteListener) {
            val popup = PopupMenu(view.context, view)
            val inflater: MenuInflater = popup.menuInflater
            inflater.inflate(R.menu.list_popup, popup.menu)
            popup.setOnMenuItemClickListener {
                if (it.itemId == R.id.delete_item) {
                    delListener.deleteDevice(position)
                    return@setOnMenuItemClickListener true
                }

                return@setOnMenuItemClickListener false
            }
            popup.show()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemDeviceBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding)
            }
        }
    }

    interface ItemDeleteListener {

        fun deleteDevice(position: Int)

    }
}