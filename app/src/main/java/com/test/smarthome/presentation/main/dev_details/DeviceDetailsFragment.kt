package com.test.smarthome.presentation.main.dev_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.test.smarthome.databinding.FragmentDetailsBinding

class DeviceDetailsFragment : Fragment() {

    private lateinit var binding: FragmentDetailsBinding
    private lateinit var deviceDetailsViewModel: DeviceDetailsViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        deviceDetailsViewModel =
                ViewModelProvider(this).get(DeviceDetailsViewModel::class.java)
        binding = FragmentDetailsBinding.inflate(inflater, container, false)

        return binding.root
    }
}