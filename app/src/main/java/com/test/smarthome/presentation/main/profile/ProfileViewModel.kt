package com.test.smarthome.presentation.main.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.test.smarthome.data.models.User
import com.test.smarthome.domain.GetProfileUseCase
import com.test.smarthome.domain.UpdateUserProfileUseCase
import kotlinx.coroutines.launch

class ProfileViewModel(
    private val getProfileUseCase: GetProfileUseCase,
    private val updateUserProfileUseCase: UpdateUserProfileUseCase
) : ViewModel() {

    private val _user = MutableLiveData<User>()
    val user: LiveData<User> = _user

    fun getProfile() {
        viewModelScope.launch {
            val result = getProfileUseCase.invoke()
            _user.postValue(result)
        }
    }

    fun saveProfile() {
        viewModelScope.launch {
            _user.value?.let {
                updateUserProfileUseCase.invoke(it)
            }
        }
    }

}