package com.test.smarthome.presentation.main

import android.os.Bundle
import android.view.View
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.forEach
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.test.smarthome.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_SmartHomeApp)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navView: BottomNavigationView = findViewById(R.id.nav_view)
        val navController: NavController = findNavController(R.id.nav_host_fragment)

        bind(navController, navView)

        val appBarConfiguration = AppBarConfiguration(setOf(
                R.id.navigation_home, R.id.navigation_profile))
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }

    fun bind(navigationController: NavController, bottomNavigationView: BottomNavigationView) {
        navigationController.addOnDestinationChangedListener { _, destination, _ ->
            this.hideBottomNavigation(bottomNavigationView)

            bottomNavigationView.menu.forEach {
                if (it.itemId == destination.id) {
                    this.hideBottomNavigation(bottomNavigationView, false)
                }
            }
        }
    }

    private fun hideBottomNavigation(bottomNavigationView: BottomNavigationView,
                                     hide: Boolean = true) {
        bottomNavigationView.visibility = if (hide) View.GONE else View.VISIBLE
    }

}