package com.test.smarthome.presentation.main.dev_details.roller

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.transition.TransitionInflater
import com.test.smarthome.R
import com.test.smarthome.data.models.RollerShutter
import com.test.smarthome.databinding.FragmentRollerControlBinding
import com.test.smarthome.util.getViewModelFactory

class RollerFragment : Fragment() {
    private lateinit var binding: FragmentRollerControlBinding
    private val viewModel by viewModels<RollerViewModel> { getViewModelFactory() }
    private val args: RollerFragmentArgs by navArgs()

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRollerControlBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        viewModel.getDevice(args.deviceId)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition = TransitionInflater
            .from(context).inflateTransition(android.R.transition.move)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        updateIcon()
        setTransitions()

        with(viewModel) {
            device.observe(viewLifecycleOwner, Observer {
                if (it is RollerShutter) {
                    binding.vsbPosition.progress = it.position
                    binding.header.tvDevName.text = it.deviceName
                    binding.header.tvDevId.text = it.id.toString()
                }
            })
        }

        binding.vsbPosition.setOnReleaseListener {
            viewModel.updateRollerPosition(it)
        }
    }

    private fun setTransitions() {
        ViewCompat.setTransitionName(binding.header.ivDevIcon, "icon_${args.deviceId}")
        ViewCompat.setTransitionName(binding.header.tvDevName, "name_${args.deviceId}")
        ViewCompat.setTransitionName(binding.header.tvDevId, "id_${args.deviceId}")
        ViewCompat.setTransitionName(binding.header.cvRoot, "root_${args.deviceId}")
    }

    private fun updateIcon() {
        binding.header.ivDevIcon.setImageDrawable(
            ContextCompat.getDrawable(
                requireContext(),
                R.drawable.ic_roller)
        )
    }

}