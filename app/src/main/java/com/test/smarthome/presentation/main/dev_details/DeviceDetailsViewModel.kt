package com.test.smarthome.presentation.main.dev_details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.test.smarthome.data.models.Device
import com.test.smarthome.data.models.Result
import com.test.smarthome.domain.GetDeviceUseCase
import kotlinx.coroutines.launch

open class DeviceDetailsViewModel(
    private val getDeviceUseCase: GetDeviceUseCase
) : ViewModel() {

    private val _device = MutableLiveData<Device>()
    val device: LiveData<Device> = _device

    fun getDevice(deviceId: String) {
        viewModelScope.launch {
            val taskResult = getDeviceUseCase(deviceId)
            if (taskResult is Result.Success) {
                _device.postValue(taskResult.data)
            } else {
                //
            }
        }
    }

}