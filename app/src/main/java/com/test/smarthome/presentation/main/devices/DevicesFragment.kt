package com.test.smarthome.presentation.main.devices

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.RecyclerView
import com.test.smarthome.R
import com.test.smarthome.databinding.FragmentDevicesBinding
import com.test.smarthome.util.getViewModelFactory

class DevicesFragment : Fragment() {

    private val viewModel by viewModels<DevicesViewModel> { getViewModelFactory() }
    private lateinit var binding: FragmentDevicesBinding

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDevicesBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.rvDevices.apply {
            adapter = viewModel.devAdapter
            postponeEnterTransition()
            viewTreeObserver.addOnPreDrawListener {
                startPostponedEnterTransition()
                true
            }
        }
    }

    override fun onStart() {
        super.onStart()

        viewModel.fetchDevices()
    }
}