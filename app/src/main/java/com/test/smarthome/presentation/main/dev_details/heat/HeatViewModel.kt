package com.test.smarthome.presentation.main.dev_details.heat

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.test.smarthome.data.models.Heater
import com.test.smarthome.data.models.Modes
import com.test.smarthome.data.models.TEMP_STEP
import com.test.smarthome.domain.GetDeviceUseCase
import com.test.smarthome.domain.UpdateDeviceUseCase
import com.test.smarthome.presentation.main.dev_details.DeviceDetailsViewModel
import kotlinx.coroutines.launch

class HeatViewModel(
    private val getDeviceUseCase: GetDeviceUseCase,
    private val updateDeviceUseCase: UpdateDeviceUseCase
) : DeviceDetailsViewModel(getDeviceUseCase) {

    val tempChangeListener = object : OnSeekBarChangedListener {
        private val step = TEMP_STEP

        override fun onChanged(progress: Int) {
            updateDisplayTemperature(getTempFrom(progress))
        }

        override fun onDraggingStopped(progress: Int) {
            getTempFrom(progress).let {
                updateDisplayTemperature(it)
                updateTemperature(it)
            }
        }

        override fun getStep() = step
    }

    private val _displayTemp = MutableLiveData<String>()
    val displayTemp: LiveData<String> = _displayTemp

    fun updateHeaterMode(isOn: Boolean) {
        viewModelScope.launch {
            (device.value as? Heater)?.let {
                it.modes = if (isOn) Modes.ON else Modes.OFF
                updateDeviceUseCase.invoke(it)
            }
        }
    }

    fun updateDisplayTemperature(progress: Int) {
        _displayTemp.value = "${progress / 10}.${progress % 10}"
    }

    fun updateTemperature(progress: Int) {
        viewModelScope.launch {
            (device.value as? Heater)?.let {
                it.temperature = progress.toDouble().div(10.0)
                updateDeviceUseCase.invoke(it)
            }
        }
    }

    fun getTempFrom(progress: Int) =
        (progress % TEMP_STEP).let {
            when {
                it > TEMP_STEP / 2 -> progress + (TEMP_STEP - it)
                else -> progress - it
            }
        }

    interface OnSeekBarChangedListener {
        fun onChanged(progress: Int)
        fun getStep(): Int
        fun onDraggingStopped(progress: Int)
    }

}