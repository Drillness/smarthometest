package com.test.smarthome.presentation.main.dev_details.roller

import androidx.lifecycle.viewModelScope
import com.test.smarthome.data.models.RollerShutter
import com.test.smarthome.domain.GetDeviceUseCase
import com.test.smarthome.domain.UpdateDeviceUseCase
import com.test.smarthome.presentation.main.dev_details.DeviceDetailsViewModel
import kotlinx.coroutines.launch

class RollerViewModel(
    private val getDeviceUseCase: GetDeviceUseCase,
    private val updateDeviceUseCase: UpdateDeviceUseCase
) : DeviceDetailsViewModel(getDeviceUseCase) {

    fun updateRollerPosition(position: Int) {
        viewModelScope.launch {
            (device.value as? RollerShutter)?.let {
                it.position = position
                updateDeviceUseCase.invoke(it)
            }
        }
    }

}