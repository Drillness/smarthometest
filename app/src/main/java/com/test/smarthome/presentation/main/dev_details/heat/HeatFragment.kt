package com.test.smarthome.presentation.main.dev_details.heat

import android.animation.Animator
import android.animation.ObjectAnimator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import android.widget.SeekBar
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.transition.TransitionInflater
import com.test.smarthome.R
import com.test.smarthome.data.models.Heater
import com.test.smarthome.data.models.MIN_TEMP
import com.test.smarthome.data.models.Modes
import com.test.smarthome.databinding.FragmentHeatControlBinding
import com.test.smarthome.util.getViewModelFactory
import kotlin.math.roundToInt

class HeatFragment : Fragment() {

    private lateinit var binding: FragmentHeatControlBinding
    private val viewModel by viewModels<HeatViewModel> { getViewModelFactory() }

    private val args: HeatFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition = TransitionInflater
            .from(context).inflateTransition(android.R.transition.move)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHeatControlBinding.inflate(layoutInflater, container, false)

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        viewModel.getDevice(args.deviceId)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        updateIcon()
        setTransitions()

        with(viewModel) {
            device.observe(viewLifecycleOwner, Observer {
                if (it is Heater) {
                    binding.scMode.isChecked = it.modes == Modes.ON
                    binding.sbTemperature.progress = (it.temperature * 10.0).toInt()
                    binding.header.tvDevName.text = it.deviceName
                    binding.header.tvDevId.text = it.id.toString()
                }
            })

            displayTemp.observe(viewLifecycleOwner, Observer {
                binding.tvTemperature.text = getString(R.string.degrees, it)
            })
        }

        binding.scMode.setOnCheckedChangeListener { _, isChecked ->
            viewModel.updateHeaterMode(isChecked)
        }

        binding.sbTemperature.seekBarInterceptor()
    }

    private fun SeekBar.seekBarInterceptor() {
        setOnSeekBarChangeListener(
        object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(
                seekBar: SeekBar?,
                changedProgress: Int,
                fromUser: Boolean
            ) {
                if (changedProgress < MIN_TEMP) {
                    progress = MIN_TEMP
                } else {
                    viewModel.tempChangeListener.onChanged(changedProgress)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                seekBar?.let { bar ->
                    viewModel.tempChangeListener?.let { changedListener ->
                        val progress = bar.progress
                        val step = changedListener.getStep()
                        if ((progress % step) != 0) {
                            // Calculate closest
                            var nearestAvailable =
                                ((progress.toFloat()) / (step.toFloat())).roundToInt() * step

                            val anim = ObjectAnimator.ofInt(
                                bar,
                                "progress",
                                progress,
                                nearestAvailable
                            )

                            anim.duration = 100
                            anim.interpolator = DecelerateInterpolator();
                            anim.addListener(object : Animator.AnimatorListener {
                                override fun onAnimationRepeat(animation: Animator?) {
                                    // Not needed
                                }

                                override fun onAnimationEnd(animation: Animator?) {
                                    viewModel.tempChangeListener.onDraggingStopped(nearestAvailable)
                                }

                                override fun onAnimationCancel(animation: Animator?) {
                                    // Not needed
                                }

                                override fun onAnimationStart(animation: Animator?) {
                                    // Not needed
                                }
                            })
                            anim.start()
                        } else {
                            viewModel.tempChangeListener.onDraggingStopped(progress)
                        }
                    }
                }
            }
        }
        )
    }


    private fun setTransitions() {
        ViewCompat.setTransitionName(binding.header.ivDevIcon, "icon_${args.deviceId}")
        ViewCompat.setTransitionName(binding.header.tvDevName, "name_${args.deviceId}")
        ViewCompat.setTransitionName(binding.header.tvDevId, "id_${args.deviceId}")
        ViewCompat.setTransitionName(binding.header.cvRoot, "root_${args.deviceId}")
    }

    private fun updateIcon() {
        binding.header.ivDevIcon.setImageDrawable(ContextCompat.getDrawable(
            requireContext(),
            R.drawable.ic_smart_heating)
        )
    }

}