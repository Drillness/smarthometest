package com.test.smarthome.presentation.main.dev_details.light

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.transition.TransitionInflater
import com.test.smarthome.R
import com.test.smarthome.data.models.Light
import com.test.smarthome.data.models.Modes
import com.test.smarthome.databinding.FragmentLightControlBinding
import com.test.smarthome.util.getViewModelFactory

class LightFragment : Fragment() {
    private lateinit var binding: FragmentLightControlBinding
    private val viewModel by viewModels<LightViewModel> { getViewModelFactory() }
    private val args: LightFragmentArgs by navArgs()

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLightControlBinding.inflate(layoutInflater, container, false)

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition = TransitionInflater
            .from(context).inflateTransition(android.R.transition.move)
    }

    override fun onStart() {
        super.onStart()
        viewModel.getDevice(args.deviceId)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        updateIcon()
        setTransitions()

        with(viewModel) {
            device.observe(viewLifecycleOwner, Observer {
                if (it is Light) {
                    binding.scMode.isChecked = it.modes == Modes.ON
                    binding.sbLightIntensity.progress = it.intensity
                    binding.header.tvDevName.text = it.deviceName
                    binding.header.tvDevId.text = it.id.toString()
                }
            })
        }

        binding.scMode.setOnCheckedChangeListener { _, isChecked ->
            viewModel.updateLightMode(isChecked)
        }

        binding.sbLightIntensity.setOnSeekBarChangeListener(object :
            SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                //
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
               //
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                seekBar?.progress?.let {
                    viewModel.updateLightIntensity(it)
                }
            }
        })
    }

    private fun setTransitions() {
        ViewCompat.setTransitionName(binding.header.ivDevIcon, "icon_${args.deviceId}")
        ViewCompat.setTransitionName(binding.header.tvDevName, "name_${args.deviceId}")
        ViewCompat.setTransitionName(binding.header.tvDevId, "id_${args.deviceId}")
        ViewCompat.setTransitionName(binding.header.cvRoot, "root_${args.deviceId}")
    }

    private fun updateIcon() {
        binding.header.ivDevIcon.setImageDrawable(
            ContextCompat.getDrawable(
            requireContext(),
            R.drawable.ic_light_bulb)
        )
    }

}