package com.test.smarthome.presentation.main.dev_details.light

import androidx.lifecycle.viewModelScope
import com.test.smarthome.data.models.Light
import com.test.smarthome.data.models.Modes
import com.test.smarthome.domain.GetDeviceUseCase
import com.test.smarthome.domain.UpdateDeviceUseCase
import com.test.smarthome.presentation.main.dev_details.DeviceDetailsViewModel
import kotlinx.coroutines.launch

class LightViewModel(
    private val getDeviceUseCase: GetDeviceUseCase,
    private val updateDeviceUseCase: UpdateDeviceUseCase
): DeviceDetailsViewModel(
    getDeviceUseCase
) {

    fun updateLightIntensity(intensity: Int) {
        viewModelScope.launch {
            (device.value as? Light)?.let {
                it.intensity = intensity
                updateDeviceUseCase.invoke(it)
            }
        }
    }

    fun updateLightMode(isOn: Boolean) {
        viewModelScope.launch {
            (device.value as? Light)?.let {
                it.modes = if (isOn) Modes.ON else Modes.OFF
                updateDeviceUseCase.invoke(it)
            }
        }
    }

}