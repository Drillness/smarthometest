package com.test.smarthome.util;

import java.util.regex.Pattern


object InputUtils {
        private const val CHECK_EMAIL = "^[\\w.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
        private const val CHECK_POSTAL_CODE = "(?i)^[a-z0-9][a-z0-9\\- ]{0,10}[a-z0-9]\$"

        fun isEmailValid(email: String): Boolean {
                if (email.isEmpty()) return true
                val pattern = Pattern.compile(CHECK_EMAIL, Pattern.CASE_INSENSITIVE)
                val matcher = pattern.matcher(email)
                return matcher.matches()
        }

        fun isNameValid(field: String) = field.isNotBlank()
        fun isCityValid(field: String) = field.isNotBlank()
        fun isCountryValid(field: String) = field.isNotBlank()
        fun isStreet(field: String) = field.isNotBlank()

        fun isPostalCodeValid(postalCode: String): Boolean {
                if (postalCode.isEmpty()) return true
                val pattern = Pattern.compile(CHECK_POSTAL_CODE, Pattern.CASE_INSENSITIVE)
                val matcher = pattern.matcher(postalCode)
                return matcher.matches()
        }

}
