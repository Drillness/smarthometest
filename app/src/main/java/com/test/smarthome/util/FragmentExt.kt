package com.test.smarthome.util
/**
 * Extension functions for Fragment.
 */

import androidx.fragment.app.Fragment
import com.test.smarthome.SmartHomeApp
import com.test.smarthome.data.di.ViewModelFactory

fun Fragment.getViewModelFactory(): ViewModelFactory {
    val repository = (requireContext().applicationContext as SmartHomeApp).deviceRepo
    return ViewModelFactory(repository)
}
