
package com.test.smarthome.data.source.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.test.smarthome.data.models.devices.DeviceEntity
import com.test.smarthome.data.models.devices.UserEntity

/**
 * The Room Database that contains the Task table.
 *
 * Note that exportSchema should be true in production databases.
 */
@Database(entities = [DeviceEntity::class, UserEntity::class], version = 1, exportSchema = false)
abstract class SmartHomeDatabase : RoomDatabase() {

    abstract fun taskDao(): SmartHomeDao
}
