package com.test.smarthome.data.models

enum class DeviceType {
    UNKNOWN,
    LIGHT,
    HEATER,
    ROLLER_SHUTTER;

    override fun toString(): String {
        return when (this) {
            UNKNOWN -> "Unknown"
            LIGHT -> "Light"
            HEATER -> "Heater"
            ROLLER_SHUTTER -> "RollerShutter"
        }
    }

    companion object {
        fun fromString(type: String): DeviceType {
            return if (type.equals(LIGHT.toString(), ignoreCase = true))
                LIGHT
            else if (type.equals(HEATER.toString(), ignoreCase = true))
                HEATER
            else if (type.equals(ROLLER_SHUTTER.toString(), ignoreCase = true))
                ROLLER_SHUTTER
            else
                UNKNOWN
        }
    }

}