package com.test.smarthome.data.source.remote

import com.test.smarthome.data.source.remote.responses.FetchResponse
import retrofit2.http.GET

interface SmartHomeApi {

    @GET("/modulotest/data.json")
    suspend fun fetchDevices(): FetchResponse

}