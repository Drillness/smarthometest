package com.test.smarthome.data.source.remote

import com.test.smarthome.data.deviceModelToResponse
import com.test.smarthome.data.models.Device
import com.test.smarthome.data.models.Result
import com.test.smarthome.data.models.Result.Error
import com.test.smarthome.data.models.Result.Success
import com.test.smarthome.data.models.User
import com.test.smarthome.data.models.UserWithDevs
import com.test.smarthome.data.source.DeviceDataSource
import com.test.smarthome.data.toUser
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DeviceRemoteDataSource internal constructor(
    private val devicesApi: SmartHomeApi,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : DeviceDataSource {

    override suspend fun getUserWithDevs(): Result<UserWithDevs> = withContext(ioDispatcher) {
        return@withContext try {
            val resp = devicesApi
                .fetchDevices()
            val user = resp.userResponse.toUser()
            Success(
                UserWithDevs(
                    resp.devices.map(deviceModelToResponse), user
                )
            )
        } catch (e: Exception) {
            Error(e)
        }
    }

    override suspend fun saveUser(user: User) {
        TODO("Not yet implemented")
    }

    override suspend fun getDevices(): Result<List<Device>> {
        TODO("Not yet implemented")
    }

    override suspend fun saveUserWithDevs(user: UserWithDevs) {
        TODO("Not needed")
    }

    override suspend fun saveDevices(devs: List<Device>) {
        TODO("Not needed")
    }

    override suspend fun getDevice(deviceId: String): Result<Device> {
        TODO("Not needed")
    }

    override suspend fun deleteDevice(deviceId: String) = withContext(ioDispatcher) {
        TODO("Not needed")
    }

    override suspend fun updateDevice(device: Device) {
        TODO("Not needed")
    }

}
