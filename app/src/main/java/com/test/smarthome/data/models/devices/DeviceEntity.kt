package com.test.smarthome.data.models.devices

import androidx.room.*
import com.test.smarthome.data.models.USER


// For simplicity, user Id was hardcoded
@Entity(
    tableName = "Devices",
    foreignKeys = [ForeignKey(
        entity = UserEntity::class,
        parentColumns = ["userId"],
        childColumns = ["userId"],
        onDelete = ForeignKey.CASCADE
    )
    ]
)
data class DeviceEntity @JvmOverloads constructor(
    @ColumnInfo(name = "productType") var productType: String = "",
    @ColumnInfo(name = "deviceName") var deviceName: String = "",
    @ColumnInfo(name = "mode") var mode: String? = null,
    @ColumnInfo(name = "intensity") var intensity: Int? = null,
    @ColumnInfo(name = "position") var position: Int? = null,
    @ColumnInfo(name = "temperature") var temperature: Double? = null,
    @ColumnInfo(name = "userId") var userId: String? = USER,
    @PrimaryKey @ColumnInfo(name = "externalId") var externalId: Int = 0
)

@Entity(tableName = "User")
data class UserEntity @JvmOverloads constructor(
    @ColumnInfo(name = "firstName") var firstName: String = "",
    @ColumnInfo(name = "lastName") var lastName: String = "",
    @ColumnInfo(name = "city") var city: String = "",
    @ColumnInfo(name = "postalCode") var postalCode: Long = 0,
    @ColumnInfo(name = "street") var street: String = "",
    @ColumnInfo(name = "streetCode") var streetCode: String = "",
    @ColumnInfo(name = "country") var country: String = "",
    @ColumnInfo(name = "birthDate") var birthDate: Long = 0,
    @PrimaryKey @ColumnInfo(name = "userId") var id: String = USER
)

class UserWithDevicesEntity {
    @Embedded
    var user: UserEntity? = null

    @Relation(parentColumn = "userId", entity = DeviceEntity::class, entityColumn = "userId")
    var devs: List<DeviceEntity>? = null
}