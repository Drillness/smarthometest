package com.test.smarthome.data.models

enum class Modes {
    ON,
    OFF;

    override fun toString() = if (this == ON) "On" else "Off"

    companion object {

        fun fromString(mode: String) =
            if (mode.equals("on", ignoreCase = true))
                ON
        else
            OFF

    }

}