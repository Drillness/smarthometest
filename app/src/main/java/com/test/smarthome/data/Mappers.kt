package com.test.smarthome.data

import com.test.smarthome.data.models.*
import com.test.smarthome.data.models.devices.DeviceEntity
import com.test.smarthome.data.models.devices.UserEntity
import com.test.smarthome.data.models.devices.UserWithDevicesEntity
import com.test.smarthome.data.source.remote.responses.DeviceResponse
import com.test.smarthome.data.source.remote.responses.UserResponse

abstract class Mapper<in T, out U> private constructor() : (T) -> U {

    fun asListMapper(): Mapper<List<T>?, List<U>> = object : Mapper<List<T>?, List<U>>() {

        override fun invoke(t: List<T>?) = t?.map(this@Mapper) ?: emptyList()
    }

    companion object {

        fun <T, U> build(action: T.() -> U): Mapper<T, U> = object : Mapper<T, U>() {
            override fun invoke(t: T) = action(t)
        }
    }
}

val deviceEntityToDevice = Mapper.build<DeviceEntity, Device> {
    when (DeviceType.fromString(this.productType)) {
        DeviceType.HEATER -> Heater(
            id = this.externalId,
            deviceName = this.deviceName,
            modes = Modes.fromString(this.mode ?: "OFF"),
            temperature = this.temperature ?: 0.0
        )
        DeviceType.LIGHT -> Light(
            id = this.externalId,
            deviceName = this.deviceName,
            modes = Modes.fromString(this.mode ?: "OFF"),
            intensity = this.intensity ?: 0
        )
        DeviceType.ROLLER_SHUTTER -> RollerShutter(
            id = this.externalId,
            deviceName = this.deviceName,
            position = this.position ?: 0
        )
        DeviceType.UNKNOWN -> UnknownDevice(
            id = this.externalId,
            deviceName = this.deviceName
        )
    }
}

val deviceToEntity = Mapper.build<Device, DeviceEntity> {
    when (true) {
        (this is Heater) -> DeviceEntity(
            externalId = this.id,
            deviceName = this.deviceName,
            mode = modes.toString(),
            temperature = this.temperature,
            productType = this.productType.toString()
        )
        (this is Light) -> DeviceEntity(
            externalId = this.id,
            deviceName = this.deviceName,
            mode = modes.toString(),
            intensity = this.intensity,
            productType = this.productType.toString()
        )
        (this is RollerShutter) -> DeviceEntity(
            externalId = this.id,
            deviceName = this.deviceName,
            position = this.position,
            productType = this.productType.toString()
        )
        else -> DeviceEntity(
            externalId = this.id,
            deviceName = this.deviceName,
            productType = this.productType.toString()
        )
    }
}

val deviceModelToResponse = Mapper.build<DeviceResponse, Device> {
    when (DeviceType.fromString(this.productType)) {
        DeviceType.HEATER -> Heater(
            id = this.id,
            deviceName = this.deviceName,
            modes = Modes.fromString(this.mode ?: "OFF"),
            temperature = this.temperature ?: 0.0
        )
        DeviceType.LIGHT -> Light(
            id = this.id,
            deviceName = this.deviceName,
            modes = Modes.fromString(this.mode ?: "OFF"),
            intensity = this.intensity ?: 0
        )
        DeviceType.ROLLER_SHUTTER -> RollerShutter(
            id = this.id,
            deviceName = this.deviceName,
            position = this.position ?: 0
        )
        DeviceType.UNKNOWN -> UnknownDevice(
            id = this.id,
            deviceName = this.deviceName
        )
    }
}

fun UserEntity.toUser() =
    User(
        firstName,
        lastName,
        Address(
            city,
            postalCode,
            street,
            streetCode,
            country
        ),
        birthDate
    )

fun User.toEntity() =
    UserEntity(
        firstName,
        lastName,
        addressResponse.city,
        addressResponse.postalCode,
        addressResponse.street,
        addressResponse.streetCode,
        addressResponse.country,
        birthDate
    )

fun UserResponse.toUser() =
    User(
        firstName,
        lastName,
        Address(
            addressResponse.city,
            addressResponse.postalCode,
            addressResponse.street,
            addressResponse.streetCode,
            addressResponse.country
        ),
        birthDate
    )


fun UserWithDevs.toEntity(): UserWithDevicesEntity {
    val uwd = UserWithDevicesEntity()
    uwd.devs = devices.map(deviceToEntity)
    uwd.user = userResponse.toEntity()

    return uwd
}

fun UserWithDevicesEntity.toModel(): UserWithDevs {
    return UserWithDevs(
        devs?.map(deviceEntityToDevice) ?: emptyList(),
        user?.toUser()?: User()
    )
}
