package com.test.smarthome.data.source

import com.test.smarthome.data.models.Device
import com.test.smarthome.data.models.Result
import com.test.smarthome.data.models.User
import com.test.smarthome.data.models.UserWithDevs

interface DeviceDataSource {

    suspend fun getUserWithDevs(): Result<UserWithDevs>

    suspend fun saveUserWithDevs(user: UserWithDevs)

    suspend fun getDevices(): Result<List<Device>>

    suspend fun getDevice(deviceId: String): Result<Device>

    suspend fun deleteDevice(deviceId: String)

    suspend fun saveDevices(devs: List<Device>)

    suspend fun updateDevice(device: Device)

    suspend fun saveUser(user: User)

}
