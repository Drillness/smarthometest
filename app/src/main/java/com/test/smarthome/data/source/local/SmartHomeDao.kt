package com.test.smarthome.data.source.local

import androidx.room.*
import com.test.smarthome.data.models.devices.DeviceEntity
import com.test.smarthome.data.models.devices.UserEntity
import com.test.smarthome.data.models.devices.UserWithDevicesEntity


@Dao
interface SmartHomeDao {

    @Transaction
    @Query("SELECT * FROM User WHERE userId = :userId")
    fun getData(userId: String?): UserWithDevicesEntity?

    @Transaction
    fun insert(dialogWithTagsEntity: UserWithDevicesEntity) {
        insert(dialogWithTagsEntity.user)
        for (tag in dialogWithTagsEntity.devs!!) {
            insert(tag)
        }
    }

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(dialog: UserEntity?): Long

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(tag: DeviceEntity?): Long

    @Query("SELECT * FROM Devices")
    suspend fun getDevices(): List<DeviceEntity>

    @Query("SELECT * FROM Devices WHERE externalId = :deviceId")
    suspend fun getDeviceById(deviceId: String): DeviceEntity?

    @Query("DELETE FROM Devices WHERE externalId = :deviceId")
    suspend fun deleteDeviceById(deviceId: String): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(devices: List<DeviceEntity?>?)

    @Update
    fun updateDevice(deviceEntity: DeviceEntity)

    @Update
    fun updateUser(user: UserEntity)

}
