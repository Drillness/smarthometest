package com.test.smarthome.data.di

import android.content.Context
import androidx.annotation.VisibleForTesting
import androidx.room.Room
import com.test.smarthome.data.source.DefaultDevicesRepository
import com.test.smarthome.data.source.DeviceDataSource
import com.test.smarthome.data.source.DevicesRepository
import com.test.smarthome.data.source.local.DeviceLocalDataSource
import com.test.smarthome.data.source.local.SmartHomeDatabase
import com.test.smarthome.data.source.remote.DeviceRemoteDataSource
import com.test.smarthome.data.source.remote.SmartHomeApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object RepoProvider {
    private var database: SmartHomeDatabase? = null
    @Volatile
    var tasksRepository: DevicesRepository? = null
        @VisibleForTesting set

    fun provideTasksRepository(context: Context): DevicesRepository {
        synchronized(this) {
            return tasksRepository ?: tasksRepository ?: createTasksRepository(context)
        }
    }

    private fun createTasksRepository(context: Context): DevicesRepository {
        val prefs = context.getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE)
        return DefaultDevicesRepository(
            deviceRemoteDataSource = createRemoteDataSource(),
            deviceLocalDataSource = createTaskLocalDataSource(context),
            prefs = prefs
        )
    }

    private fun createTaskLocalDataSource(context: Context): DeviceDataSource {
        val database = database ?: createDataBase(context)
        return DeviceLocalDataSource(database.taskDao())
    }

    private fun createRemoteDataSource(): DeviceDataSource {
        return DeviceRemoteDataSource(provideApi())
    }

    private fun provideApi(): SmartHomeApi {
        return provideRetrofit().create(SmartHomeApi::class.java)
    }

    private fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
            .client(provideOkHTTPClient())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("http://storage42.com/")
            .build()
    }

    private fun provideOkHTTPClient(): OkHttpClient {
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        return OkHttpClient.Builder()
            .addInterceptor(logging)
            .build()
    }

    private fun createDataBase(context: Context): SmartHomeDatabase {
        val result = Room.databaseBuilder(
            context.applicationContext,
            SmartHomeDatabase::class.java, "SmartHomeDatabase.db"
        ).build()
        database = result
        return result
    }
}