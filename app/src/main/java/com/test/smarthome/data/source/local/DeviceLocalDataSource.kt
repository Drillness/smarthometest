package com.test.smarthome.data.source.local

import com.test.smarthome.data.deviceEntityToDevice
import com.test.smarthome.data.deviceToEntity
import com.test.smarthome.data.models.*
import com.test.smarthome.data.models.Result.Error
import com.test.smarthome.data.models.Result.Success
import com.test.smarthome.data.models.devices.UserWithDevicesEntity
import com.test.smarthome.data.source.DeviceDataSource
import com.test.smarthome.data.toEntity
import com.test.smarthome.data.toModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DeviceLocalDataSource internal constructor(
    private val smartHomeDao: SmartHomeDao,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : DeviceDataSource {

    override suspend fun getDevices(): Result<List<Device>> = withContext(ioDispatcher) {
        return@withContext try {
            Success(smartHomeDao.getData(USER)!!.devs!!.map(deviceEntityToDevice) )
        } catch (e: Exception) {
            Error(e)
        }
    }

    override suspend fun getUserWithDevs(): Result<UserWithDevs> = withContext(ioDispatcher) {
        return@withContext try {
            Success(smartHomeDao.getData(USER)!!.toModel())
        } catch (e: Exception) {
            Error(e)
        }
    }

    override suspend fun updateDevice(device: Device) {
        val dev = listOf(device)
        smartHomeDao.updateDevice(dev.map(deviceToEntity).first())
    }

    override suspend fun getDevice(deviceId: String): Result<Device> = withContext(ioDispatcher) {
        return@withContext try {
            val dev = listOf(smartHomeDao.getDeviceById(deviceId)!!)
            val device = dev.map(deviceEntityToDevice).first()
            Success(
                device
            )
        } catch (e: Exception) {
            Error(e)
        }
    }

    override suspend fun saveUser(user: User) {
        smartHomeDao.updateUser(user.toEntity())
    }

    override suspend fun saveUserWithDevs(user: UserWithDevs) {
        smartHomeDao.insert(user.toEntity())
    }

    override suspend fun saveDevices(devs: List<Device>) = withContext(ioDispatcher) {
        smartHomeDao.insertAll(devs.map(deviceToEntity))
    }

    override suspend fun deleteDevice(deviceId: String) = withContext<Unit>(ioDispatcher) {
        smartHomeDao.deleteDeviceById(deviceId)
    }
}
