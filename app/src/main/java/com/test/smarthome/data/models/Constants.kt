package com.test.smarthome.data.models

const val MIN_TEMP = 70
const val MAX_TEMP = 280
const val TEMP_STEP = 5
const val USER = "USER"