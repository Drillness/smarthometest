package com.test.smarthome.data.source

import android.content.SharedPreferences
import com.test.smarthome.data.models.*
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class DefaultDevicesRepository(
    private val deviceRemoteDataSource: DeviceDataSource,
    private val deviceLocalDataSource: DeviceDataSource,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO,
    private val prefs: SharedPreferences
) : DevicesRepository {

    override suspend fun getDevices(): Result<List<Device>> {
        return withContext(ioDispatcher) {
            if (!prefs.getBoolean(DATA_FETCHED_KEY, false)) {
                val devs = deviceRemoteDataSource.getUserWithDevs()

                if (devs is Result.Success) {
                    saveDevices(devs.data)

                    prefs.edit().putBoolean(DATA_FETCHED_KEY, true).apply()
                }

                return@withContext Result.Success(emptyList<Device>())
            } else {
                val dev = deviceLocalDataSource.getUserWithDevs()
                val list = if (dev is Result.Success) dev.data.devices else emptyList()
                return@withContext Result.Success(list)
            }
        }
    }

    override suspend fun getUser(): User {
        return withContext(ioDispatcher) {
                val userRes = deviceLocalDataSource.getUserWithDevs()
                if (userRes is Result.Success) {
                    return@withContext userRes.data.userResponse
                } else
                    User()
        }
    }

    override suspend fun updateUser(user: User) {
        withContext(ioDispatcher) {
            launch { deviceLocalDataSource.saveUser(user) }
        }
    }

    override suspend fun updateDevice(device: Device) {
        withContext(ioDispatcher) {
            launch { deviceLocalDataSource.updateDevice(device) }
        }
    }

    override suspend fun deleteDevice(taskId: String) {
        coroutineScope {
            launch { deviceLocalDataSource.deleteDevice(taskId) }
        }
    }

    override suspend fun getDevice(taskId: String): Result<Device> {
        return withContext(ioDispatcher) {
            return@withContext deviceLocalDataSource.getDevice(taskId)
        }
    }


    private suspend fun saveDevices(userWithDevs: UserWithDevs) {
        deviceLocalDataSource.saveUserWithDevs(userWithDevs)
    }

    companion object {

        const val DATA_FETCHED_KEY = "DATA_FETCHED_KEY"

    }

}
