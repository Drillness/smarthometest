package com.test.smarthome.data.source.remote.responses

import com.google.gson.annotations.SerializedName

data class FetchResponse(
    @SerializedName("devices") val devices: List<DeviceResponse>,
    @SerializedName("user") val userResponse: UserResponse
)

data class DeviceResponse(
    @SerializedName("id") val id: Int,
    @SerializedName("deviceName") val deviceName: String,
    @SerializedName("productType") val productType: String,
    @SerializedName("temperature") val temperature: Double? = null,
    @SerializedName("intensity") val intensity: Int? = null,
    @SerializedName("position") val position: Int? = null,
    @SerializedName("mode") val mode: String? = null
)

data class UserResponse(
    @SerializedName("firstName") val firstName: String,
    @SerializedName("lastName") val lastName: String,
    @SerializedName("address") val addressResponse: AddressResponse,
    @SerializedName("birthDate") val birthDate: Long
)

data class AddressResponse(
    @SerializedName("city") val city: String = "",
    @SerializedName("postalCode") val postalCode: Long = 0,
    @SerializedName("street") val street: String = "",
    @SerializedName("streetCode") val streetCode: String = "",
    @SerializedName("country") val country: String = ""
)