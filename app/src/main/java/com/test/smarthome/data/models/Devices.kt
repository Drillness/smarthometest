package com.test.smarthome.data.models

abstract class Device(
    val id: Int,
    val deviceName: String,
    val productType: DeviceType
)

class Heater(
    id: Int,
    deviceName: String,
    var temperature: Double,
    var modes: Modes = Modes.OFF
) : Device(
    id,
    deviceName,
    DeviceType.HEATER
)

class Light(
    id: Int,
    deviceName: String,
    var intensity: Int,
    var modes: Modes = Modes.OFF
) : Device(
    id,
    deviceName,
    DeviceType.LIGHT
)

class RollerShutter(
    id: Int,
    deviceName: String,
    var position: Int
) : Device(
    id,
    deviceName,
    DeviceType.ROLLER_SHUTTER
)

class UnknownDevice(
    id: Int,
    deviceName: String
) : Device(
    id,
    deviceName,
    DeviceType.UNKNOWN
)

data class UserWithDevs(
    val devices: List<Device>,
    val userResponse: User
)

data class User(
    val firstName: String = "",
    val lastName: String = "",
    val addressResponse: Address = Address(),
    val birthDate: Long = 0
)

data class Address(
    val city: String = "",
    val postalCode: Long = 0,
    val street: String = "",
    val streetCode: String = "",
    val country: String = ""
)