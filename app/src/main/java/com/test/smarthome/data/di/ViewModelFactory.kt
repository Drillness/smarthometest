package com.test.smarthome.data.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.test.smarthome.data.source.DevicesRepository
import com.test.smarthome.domain.*
import com.test.smarthome.presentation.main.dev_details.heat.HeatViewModel
import com.test.smarthome.presentation.main.dev_details.light.LightViewModel
import com.test.smarthome.presentation.main.dev_details.roller.RollerViewModel
import com.test.smarthome.presentation.main.devices.DevicesViewModel
import com.test.smarthome.presentation.main.profile.ProfileViewModel

/**
 * Factory for all ViewModels.
 */
@Suppress("UNCHECKED_CAST")
class ViewModelFactory constructor(
    private val deviceRepo: DevicesRepository
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>) =
        with(modelClass) {
            when {
                isAssignableFrom(DevicesViewModel::class.java) ->
                    DevicesViewModel(
                        FetchDevicesUseCase(deviceRepo),
                        RemoveDevicesUseCase(deviceRepo)
                    )
                isAssignableFrom(HeatViewModel::class.java) ->
                    HeatViewModel(
                        GetDeviceUseCase(deviceRepo),
                        UpdateDeviceUseCase(deviceRepo)
                    )
                isAssignableFrom(LightViewModel::class.java) ->
                    LightViewModel(
                        GetDeviceUseCase(deviceRepo),
                        UpdateDeviceUseCase(deviceRepo)
                    )
                isAssignableFrom(RollerViewModel::class.java) ->
                    RollerViewModel(
                        GetDeviceUseCase(deviceRepo),
                        UpdateDeviceUseCase(deviceRepo)
                    )
                isAssignableFrom(ProfileViewModel::class.java) ->
                    ProfileViewModel(
                        GetProfileUseCase(deviceRepo),
                        UpdateUserProfileUseCase(deviceRepo)
                    )

                else ->
                    throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
            }
        } as T
}
