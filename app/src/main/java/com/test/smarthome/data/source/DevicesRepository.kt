package com.test.smarthome.data.source

import com.test.smarthome.data.models.Device
import com.test.smarthome.data.models.devices.DeviceEntity
import com.test.smarthome.data.models.Result
import com.test.smarthome.data.models.User

interface DevicesRepository {

    suspend fun getUser(): User

    suspend fun updateUser(user: User)

    suspend fun updateDevice(device: Device)

    suspend fun getDevices(): Result<List<Device>>

    suspend fun getDevice(taskId: String): Result<Device>

    suspend fun deleteDevice(taskId: String)

}