package com.test.smarthome.domain

import com.test.smarthome.data.models.User
import com.test.smarthome.data.source.DevicesRepository

class GetProfileUseCase(
    private val repository: DevicesRepository
) {
    suspend operator fun invoke(): User {
        return repository.getUser()
    }
}