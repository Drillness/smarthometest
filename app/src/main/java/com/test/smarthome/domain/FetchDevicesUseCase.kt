package com.test.smarthome.domain

import com.test.smarthome.data.models.Device
import com.test.smarthome.data.models.Result
import com.test.smarthome.data.source.DevicesRepository

class FetchDevicesUseCase(
    private val repository: DevicesRepository
) {

    suspend operator fun invoke(): Result<List<Device>> {
        return repository.getDevices()
    }

}