package com.test.smarthome.domain

import com.test.smarthome.data.models.Device
import com.test.smarthome.data.source.DevicesRepository

class UpdateDeviceUseCase(
    private val repository: DevicesRepository
) {
    suspend fun invoke(device: Device) {
        repository.updateDevice(device)
    }
}