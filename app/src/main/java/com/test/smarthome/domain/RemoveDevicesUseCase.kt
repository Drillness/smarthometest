package com.test.smarthome.domain

import com.test.smarthome.data.models.Device
import com.test.smarthome.data.source.DevicesRepository

class RemoveDevicesUseCase(
    private val repository: DevicesRepository
) {

    suspend operator fun invoke(device: Device) {
        repository.deleteDevice(device.id.toString())
    }

}