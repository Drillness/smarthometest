package com.test.smarthome.domain

import com.test.smarthome.data.models.Device
import com.test.smarthome.data.models.Result
import com.test.smarthome.data.source.DevicesRepository

class GetDeviceUseCase(
    private val repository: DevicesRepository
) {
    suspend operator fun invoke(devId: String): Result<Device> {
        return repository.getDevice(devId)
    }
}