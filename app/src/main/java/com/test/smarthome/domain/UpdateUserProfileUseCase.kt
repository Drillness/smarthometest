package com.test.smarthome.domain

import com.test.smarthome.data.models.User
import com.test.smarthome.data.source.DevicesRepository

class UpdateUserProfileUseCase(
    private val repository: DevicesRepository
){
    suspend operator fun invoke(user: User) {
        return repository.updateUser(user)
    }
}